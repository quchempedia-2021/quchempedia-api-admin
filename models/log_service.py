import os
import shutil


class LogService:
    """
    CLass that handles the logic behind the molecule log file management
    """

    def __init__(self, base_path):
        self.base_path = base_path

    @staticmethod
    def id_to_path(id_mol):
        """ 
           Transform an elasticsearch generated ID into a path 
        """

        path = ''

        # Parse the molecule id and define the path of the log file.
        for char in id_mol:
            path += char
            path += '/'

        return path

    def add_log_file_from_param(self, id_mol, log_file_name, log_file_data):
        """ Function for the creation of the log file when a new molecule is added
        to the database and when the log file is given in the POST request. """

        # Define the log path to add to log directory for log files.
        log_path = LogService.id_to_path(id_mol=id_mol)

        path = self.base_path + log_path

        # Create directories for the log file.
        os.makedirs(path)

        # Create the log file with the data.
        try:
            log_file = open(path + log_file_name, "x")
            for line in log_file_data:
                log_file.write(line)
        except OSError as err:
            # Delete the path created just before and raise an error if
            # the file creation failed.
            self.delete_empty_path(path)
            raise err

    def add_log_file_from_json(self, id_mol, log_file_src):
        """ Function for the creation of the log file when a new molecule is added
        to the database and when the log file is not given in the POST request. """

        # Define the log path to add to log directory for log files.
        log_path = LogService.id_to_path(id_mol=id_mol)

        path = self.base_path + log_path

        # Create directories for the log file.
        os.makedirs(path)

        # Create the log file.
        try:
            log_file = open(path + log_file_src[1], "x")
            shutil.copyfile(
                log_file_src[0] +
                '/' +
                log_file_src[1],
                path +
                log_file_src[1])
        except OSError as err:
            # Delete the file and path created just before and raise an error if
            # the file creation or the file copy failed.
            os.remove(path + log_file_src[1])
            self.delete_empty_path(path)
            raise err

    def delete_log_file(self, id_mol, log_file_name):
        """ 
            Function for the suppression of the log file when a molecule
            is deleted from the database. 
        """
        log_path = LogService.id_to_path(id_mol=id_mol)

        # log base dir + id to path (unique id/path) + filename
        log_file_path = self.base_path + log_path + log_file_name

        if os.path.exists(log_file_path):
            # remove the file
            os.remove(log_file_path)
            # trigger empty directories deletion
            self.delete_empty_path(log_path)

    def delete_empty_path(self, log_path):
        """
            Deletion of any empty folder and their parents if themselves are empty.
        """

        full_path = os.path.join(self.base_path, log_path)

        if os.path.exists(full_path) and os.path.isdir(full_path) and len(os.listdir(full_path)) == 0:
            # Extract parent folder from current path
            tmp_array = log_path.split('/')
            parent_folder = "/".join(tmp_array[:-1])

            os.rmdir(full_path)

            self.delete_empty_path(parent_folder)




