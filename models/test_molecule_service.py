import time

import pytest
import elasticsearch
from elasticsearch import Elasticsearch
from flask import Flask
import os

from .molecule_service import MoleculeService


app = Flask(__name__)

# load configuration
app.config.from_pyfile(os.path.join("..", "config/app.conf"), silent=False)
es_url = app.config.get("ES_URL")

client = Elasticsearch(es_url)
molecule_service = MoleculeService(client)


@pytest.fixture()
def valid_sample_formula():
    return "CO2"


@pytest.fixture()
def valid_sample_molecule(valid_sample_formula):
    return {
        "molecule": {
            "formula": valid_sample_formula,
        }
    }


@pytest.mark.integration
def test_add_valid_molecule(valid_sample_molecule):
    """
        Check id the molecule was added
    """
    resp = molecule_service.add(molecule=valid_sample_molecule)

    assert '_id' in resp
    assert 'result' in resp

    assert resp.get('result', '') == "created"


@pytest.mark.integration
def test_search_valid_molecule(valid_sample_formula, valid_sample_molecule):
    """
        Add a molecule
        Then Search the molecule using a formula
        Checks that at least one document was found/ no hermetic tests
    """

    molecule_service.add(molecule=valid_sample_molecule)
    time.sleep(1)
    search_resp = molecule_service.search(valid_sample_formula)

    hits = search_resp.get('hits', {})
    matches = hits.get('hits', [])

    assert "hits" in search_resp
    assert "hits" in hits

    assert type(matches) == list
    assert len(matches) != 0


@pytest.mark.integration
def test_get_valid_molecule_details(valid_sample_molecule):
    """
        Add a molecule and retrieve its id
        Then get the details of the document matching the id
        Check that the details matches the stored molecule
    """

    add_resp = molecule_service.add(molecule=valid_sample_molecule)
    mol_id = add_resp.get('_id')

    details_resp = molecule_service.get_details(mol_id)

    assert '_id' in details_resp
    assert '_source' in details_resp

    assert mol_id == details_resp.get('_id')
    assert valid_sample_molecule == details_resp.get('_source')


@pytest.mark.integration
def test_delete_valid_molecule(valid_sample_molecule):
    """
        Add a valid molecule then delete it
        Checks that elasticsearch really deleted it
    """
    # Add a molecule and return the id
    add_resp = molecule_service.add(molecule=valid_sample_molecule)
    mol_id = add_resp.get('_id')

    resp_delete = molecule_service.delete(id_mol=mol_id)

    assert '_id' in resp_delete
    assert 'result' in resp_delete

    assert resp_delete.get('_id', "") == mol_id
    assert resp_delete.get('result', "") == "deleted"


@pytest.mark.integration
def test_delete_non_existing_molecule():
    """
        Check that an error is raised
        if a non existing id were to be passed
    """

    # As the elasticsearch library is probably tested
    # this test might not have any real value

    with pytest.raises(elasticsearch.exceptions.NotFoundError):
        molecule_service.delete('fake-id')
