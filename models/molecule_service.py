# from elasticsearch_dsl import Search, Q
# from elasticsearch_dsl.connections import connections

class MoleculeService:
    def __init__(self, client):
        """
            Wraps around the elasticsearch client
            Define shared index and doc_type to avoid any copy pasted configuration problem
        """
        self.client = client
        self.index = "molecules"
        self.doc_type = "molecule"

    def search(self, formula):
        """
            Returns any molecule matching the given formula
        """

        # Define the body for the request on the Elasticsearch client.
        body = {
            "query": {
                "match_phrase": {
                    "molecule.formula": formula
                }
            }
        }

        # Try the GET request on the Elasticsearch client.
        # index is specific at the molecules documents.
        results = self.client.search(
            index=self.index,
            body=body
        )

        return results

    def get_details(self, id_mol):
        """
            Returns the details of a molecule
        """

        results = self.client.get(
            index=self.index,
            doc_type=self.doc_type,
            id=id_mol
        )

        return results

    def add(self, molecule):
        """
            Store a molecule as an elasticsearch document
            and returns the response
        """

        results = self.client.index(
            index=self.index,
            doc_type=self.doc_type,
            body=molecule
        )

        return results

    def delete(self, id_mol):
        """
            Delete the
        """

        results = self.client.delete(
            index=self.index,
            doc_type=self.doc_type,
            id=id_mol
        )

        return results
