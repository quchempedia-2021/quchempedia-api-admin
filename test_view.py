from flask import Flask
import os
import pytest
import requests
from models.log_service import LogService

app = Flask(__name__)

# load configuration
app.config.from_pyfile(os.path.join(os.getcwd(), "config", "app.conf"), silent=False)
base_url = app.config.get("BASE_URL")
root_path = app.config.get("TEST_LOG_PATH")

# define path to samples file
samples_dir = os.path.join(os.getcwd(), "samples")


@pytest.fixture()
def valid_mol_json():
    yield open(os.path.join(
        samples_dir,
        'bigone.json'
    ))


@pytest.fixture()
def valid_mol_log():
    yield open(os.path.join(
        samples_dir,
        'bigone.log'
    ))


def post_molecule_files(files):
    # Define url for the API call.
    url = base_url + 'add'
    return requests.post(url, files=files)


class TestHTTP:
    """
        Should only do http related assertion (status, body, etc...)
    """

    def test_add_molecule_with_log(self, valid_mol_json, valid_mol_log):
        """
            Test the creation of a new molecule with a right body
            verify that the response code is 201
            and that the response body contains the new molecule ID
        """

        # Call the API with POST method
        resp = post_molecule_files(files={
            "mol_json": valid_mol_json,
            "mol_log": valid_mol_log
        })
        resp_body = resp.json()

        # Validate response headers and body contents and status code.
        assert resp.status_code == 201
        assert resp_body['_id'] is not None
        assert resp_body['result'] == 'created'

    def test_add_molecule_error_without_json(self):
        """
            Test the creation of a new molecule without body
            verify that the response code is 400
            and that the response body correspond to the right error message.
        """
        resp = post_molecule_files(files={})
        resp_body = resp.json()

        # Validate response headers and body contents and status code.
        assert resp.status_code == 400
        assert resp_body['error'] == 'You must provide a body for the molecule!'

    def test_delete_molecule(self, valid_mol_json, valid_mol_log):
        """
            Test the suppression of a molecule with an existing ID
            verify that the response code is 200
            and that the response body contains the right body
        """

        # Create and get a new molecule id
        new_mol = post_molecule_files(files={
            "mol_json": valid_mol_json,
            "mol_log": valid_mol_log
        })
        id_mol = new_mol.json()['_id']

        # Define url for the API call.
        url = base_url + 'delete/' + id_mol

        # Call the API with DELETE method.
        resp = requests.delete(url)
        resp_body = resp.json()

        # Validate response headers and body contents and status code.
        assert resp.status_code == 200
        assert resp_body['_id'] == id_mol
        assert resp_body.get('result') == 'deleted'

    def test_delete_molecule_error(self):
        """
            Test the suppression of a molecule with an non existing ID
            verify that the response code is 404
            and that the response body contains the right error message.
        """
        # Define url for the API call.
        id_mol = 'fake_id_mol'
        url = f"{base_url}delete/{id_mol}"

        # Call the API with DELETE method.
        resp = requests.delete(url)
        resp_body = resp.json()

        # Validate response headers and body contents and status code.
        assert resp.status_code == 404
        assert resp_body['error'] == 'Molecule with id = \'' + \
               id_mol + '\' does not exists!'

    def test_details_molecule(self, valid_mol_json, valid_mol_log):
        """
            Test the consultation of molecule's details with an existing ID
            verify that the response code is 200
            and that the response body contains the right body and the right ID
        """
        # Create and get a new molecule id
        new_mol = post_molecule_files(files={
            "mol_json": valid_mol_json,
            "mol_log": valid_mol_log
        })
        id_mol = new_mol.json()['_id']

        # Define url for the API call.
        url = base_url + 'details/' + id_mol

        # Call the API with GET method.
        resp = requests.get(url)
        resp_body = resp.json()

        # Validate response headers and body contents and status code.
        assert resp.status_code == 200
        assert resp_body['_id'] == id_mol
        assert resp_body['found']

    def test_details_molecule_error(self):
        """
            Test the consultation of molecule's details with a non existing ID
            verify that the response code is 404
            and that the response body contains the right error message
        """
        # Define url for the API call.
        id_mol = 'fake_id_mol'
        url = base_url + 'details/' + id_mol

        # Call the API with GET method.
        resp = requests.get(url)
        resp_body = resp.json()

        # Validate response headers and body contents and status code.
        assert resp.status_code == 404
        assert resp_body['error'] == 'Molecule with id = \'' + \
               id_mol + '\' does not exists!'

    def test_search_molecule(self):
        """
            Test a search of molecules with a test formula and
            verify that the response code is 200.
        """
        # Define url for the API call.
        formula = 'test_formula'
        url = base_url + 'search/' + formula

        # Call the API with GET method.
        resp = requests.get(url)

        # Validate response headers and body contents and status code.
        assert resp.status_code == 200

    def test_wrong_route(self):
        """
            Test to call a non existing route for the API
            verify that the response code is 404
            and that the response body contains the right error message.
        """
        # Define url for the API call.
        url = base_url + 'wrong_route'

        # Call the API with GET method
        resp = requests.get(url)
        resp_body = resp.json()

        # Validate response headers and body contents and status code.
        assert resp.status_code == 404
        assert resp_body['error'] == 'Wrong url, resource not found!'


class TestInternalFileSystem:
    """
        Assertion related to internal machine state like files
        Should be launched on the machine running the app else will fail
    """

    def test_add_molecule_with_log(self, valid_mol_json, valid_mol_log):
        """
            Test the creation of a new molecule with a right body
            verify that the path was created and the log file exists
        """
        # Call the API with POST method
        resp = post_molecule_files(files={
            "mol_json": valid_mol_json,
            "mol_log": valid_mol_log
        })
        resp_body = resp.json()

        # Define the log file path.
        log_path = LogService.id_to_path(resp_body['_id'])

        path = root_path + log_path + 'bigone.log'

        # Validate if the log file has been created.
        assert os.path.isfile(path)

    def test_delete_molecule(self, valid_mol_json, valid_mol_log):
        """
            Test the suppression of a molecule with an existing ID
            verify that the log file was deleted
        """

        # Create and get a new molecule id
        new_mol = post_molecule_files(files={
            "mol_json": valid_mol_json,
            "mol_log": valid_mol_log
        })
        id_mol = new_mol.json()['_id']

        # Define the log file path.
        log_path = LogService.id_to_path(new_mol.json()['_id'])
        path = root_path + log_path + 'bigone.log'

        # Define url for the API call.
        url = base_url + 'delete/' + id_mol

        # Call the API with DELETE method.
        resp = requests.delete(url)
        resp_body = resp.json()

        # Validate if the log file has been removed.
        assert not os.path.exists(path)
